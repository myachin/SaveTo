The application allows you to save files to your device, but it cannot read your files. The
application cannot go online.

## Mirrors:

- https://notabug.org/Umnik/SaveTo
- https://gitlab.com/myachin/SaveTo
- https://gitea.myachin.xyz/umnik/SaveTo

### If you want to support me:

- ~~https://paypal.me/DMyachin~~ PP has blocked all Russians who continue to live in Russia and love
  their country.
- ~~https://liberapay.com/Umnik/~~ I can't withdraw donations from here because VISA and Mastercard
  have blocked me because I am Russian.
- https://www.tinkoff.ru/rm/myachin.dmitriy4/5wLpN86994
- monero:
  46AM2x4HaPEGUiUZCN7GxXW5g6PDGn7fWFqchGa2PBFcW7k9kuTdwpPFXTSPeibMvkW9wERfMxRQs2jY7t9aKToVRKYsYm6