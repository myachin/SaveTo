package xyz.myachin.saveto.logic.intent

import android.content.Intent
import android.os.Build
import android.provider.DocumentsContract

object IntentCreator {
    fun forDownloader(url: String) = Intent().apply {
        action = ACTION_NEED_DOWNLOAD
        type = "text/plain"
        putExtra(
            EXTRA_ADD_DOWNLOAD_TASK,
            url
        )
        return this
    }

    fun forManualSaving(intent: IncomingIntent): Intent {
        return Intent(Intent.ACTION_CREATE_DOCUMENT).apply {
            addCategory(Intent.CATEGORY_OPENABLE)
            type = intent.mimeType
            putExtra(Intent.EXTRA_TITLE, intent.suggestName())
        }
    }

    fun forDirectSaving(accessCode: Int): Intent {
        val mimeType = when (accessCode) {
            ACCESS_DOCUMENTS_CODE -> "text/plain"
            ACCESS_ARCHIVES_CODE -> "application/zip"
            ACCESS_VIDEOS_CODE -> "video/mp4"
            ACCESS_IMAGES_CODE -> "image/jpeg"
            else -> "unknown/bin"
        }

        return Intent(Intent.ACTION_OPEN_DOCUMENT_TREE).apply {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                putExtra(DocumentsContract.EXTRA_INITIAL_URI, mimeType)
            }
        }
    }

    fun removeFileIdForDownloader(removeFileId: Long): Intent {
        return Intent(ACTION_NEED_REMOVE).apply {
            putExtra(EXTRA_FILE_ID, removeFileId)
            addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
        }
    }
}
