package xyz.myachin.saveto.ui

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import xyz.myachin.saveto.R
import xyz.myachin.saveto.databinding.MainFragmentBinding
import xyz.myachin.saveto.ui.settings.MainSettingsActivity

class MainFragment : Fragment() {
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?,
    ): View {
        val binder: MainFragmentBinding =
            DataBindingUtil.inflate(inflater, R.layout.main_fragment, container, false)
        binder.settings.setOnClickListener {
            requireContext().startActivity(Intent(requireContext(),
                MainSettingsActivity::class.java))
        }
        return binder.root
    }
}