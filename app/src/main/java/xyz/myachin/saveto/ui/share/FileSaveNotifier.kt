package xyz.myachin.saveto.ui.share

interface FileSaveNotifier {
    fun onFileSaved()
}