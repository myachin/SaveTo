package xyz.myachin.saveto.ui

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import xyz.myachin.saveto.R

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }
}