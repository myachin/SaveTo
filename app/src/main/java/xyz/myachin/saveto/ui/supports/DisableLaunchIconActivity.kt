package xyz.myachin.saveto.ui.supports

import android.content.pm.PackageManager
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.google.android.material.button.MaterialButton
import com.google.android.material.snackbar.Snackbar
import xyz.myachin.saveto.R
import xyz.myachin.saveto.settings.Settings
import xyz.myachin.saveto.ui.MainActivity

class DisableLaunchIconActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_disable_launch_icon)

        val btOk: MaterialButton = findViewById(R.id.btOk)

        btOk.setOnClickListener {
            Thread {
                Settings.hideLauncherIcon = true
                Settings.versionSettings = getString(R.string.settings_version)
                Settings.setComponentState(
                    applicationContext,
                    MainActivity::class.java.name,
                    PackageManager.COMPONENT_ENABLED_STATE_DISABLED
                )
            }.start()
            Snackbar
                .make(
                    findViewById(R.id.btOk),
                    R.string.icon_disabled_now,
                    Snackbar.LENGTH_LONG
                )
                .setAnimationMode(Snackbar.ANIMATION_MODE_SLIDE)
                .show()
        }
    }
}