package xyz.myachin.saveto.ui.share.fake

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import xyz.myachin.saveto.settings.Settings
import xyz.myachin.saveto.ui.share.direct.DirectShareActivity
import xyz.myachin.saveto.ui.share.manual.ManualShareActivity

class AttachDataActivity : Activity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (intent.action != Intent.ACTION_ATTACH_DATA) {
            finish()
            return
        }

        startActivity(intent.apply {
            action = Intent.ACTION_SEND
            setClassName(packageName,
                if (Settings.attachDataManual) ManualShareActivity::class.java.name else DirectShareActivity::class.java.name)
            putExtra(Intent.EXTRA_STREAM, data)
        })
    }

    override fun onRestart() {
        super.onRestart()
        finish()
    }
}