package xyz.myachin.saveto.settings

import android.content.ComponentName
import android.content.Context
import android.content.SharedPreferences
import android.content.pm.PackageManager
import android.net.Uri
import androidx.preference.PreferenceManager
import xyz.myachin.saveto.logic.intent.CATEGORY_ARCHIVES
import xyz.myachin.saveto.logic.intent.CATEGORY_DOCUMENTS
import xyz.myachin.saveto.logic.intent.CATEGORY_IMAGES
import xyz.myachin.saveto.logic.intent.CATEGORY_VIDEOS

object Settings {
    private const val settings_version = "settings_version"
    private const val use_downloader = "use_downloader"
    private const val text_length = "text_file_name_length"
    private const val hide_launcher_icon = "hide_launcher_icon"
    private const val additional_prefs_file = "additional_prefs"

    private lateinit var mDefaultPrefs: SharedPreferences

    private lateinit var mAdditionalPrefs: SharedPreferences // this prefs will NOT backups by PlayServices
    private val editor by lazy { mDefaultPrefs.edit() }

    private val additionalEditor by lazy { mAdditionalPrefs.edit() }
    fun setup(context: Context) {
        mDefaultPrefs = PreferenceManager.getDefaultSharedPreferences(context)
        mAdditionalPrefs = context.getSharedPreferences(additional_prefs_file, Context.MODE_PRIVATE)
    }

    private fun setNewBooleanValue(key: String, value: Boolean) {
        editor.putBoolean(key, value)
        editor.apply()
    }

    var textLength: Int
        get() = mDefaultPrefs.getInt(text_length, 5)
        set(value) {
            editor.putInt(text_length, value)
            editor.apply()
        }

    var versionSettings: String
        get() = mDefaultPrefs.getString(settings_version, "")!!
        set(value) {
            editor.putString(settings_version, value)
            editor.apply()
        }

    var useDownloader: Boolean
        get() = mDefaultPrefs.getBoolean(use_downloader, true)
        set(value) = setNewBooleanValue(use_downloader, value)

    var hideLauncherIcon: Boolean
        get() = mDefaultPrefs.getBoolean(hide_launcher_icon, false)
        set(value) = setNewBooleanValue(hide_launcher_icon, value)

    fun setComponentState(context: Context, cls: String, state: Int) {
        val cmp = ComponentName.createRelative(context, cls)
        context.packageManager.setComponentEnabledSetting(cmp, state, PackageManager.DONT_KILL_APP)
    }

    fun getComponentStateEnabled(context: Context, cls: String): Boolean {
        val cmp = ComponentName.createRelative(context.applicationContext, cls)
        return (context.packageManager.getComponentEnabledSetting(cmp) == PackageManager.COMPONENT_ENABLED_STATE_ENABLED) ||
                (context.packageManager.getComponentEnabledSetting(cmp) == PackageManager.COMPONENT_ENABLED_STATE_DEFAULT)
    }

    private fun getUriValue(key: String): Uri? {
        mAdditionalPrefs.getString(key, null)?.run {
            return Uri.parse(this)
        }
        return null
    }

    var actionViewManual: Boolean
        get() = mDefaultPrefs.getBoolean("action_view_default", false)
        set(value) = setNewBooleanValue("action_view_default", value)

    var attachDataManual: Boolean
        get() = mDefaultPrefs.getBoolean("attach_data_default", false)
        set(value) = setNewBooleanValue("attach_data_default", value)

    private fun setNewUriValue(key: String, value: Uri?) {
        if (value == null) {
            additionalEditor.remove(key)
        } else {
            additionalEditor.putString(key, value.toString())
        }
        additionalEditor.apply()
    }

    var documentUri: Uri?
        get() = getUriValue(CATEGORY_DOCUMENTS)
        set(value) = setNewUriValue(CATEGORY_DOCUMENTS, value)

    var imageUri: Uri?
        get() = getUriValue(CATEGORY_IMAGES)
        set(value) = setNewUriValue(CATEGORY_IMAGES, value)

    var archiveUri: Uri?
        get() = getUriValue(CATEGORY_ARCHIVES)
        set(value) = setNewUriValue(CATEGORY_ARCHIVES, value)

    var videoUri: Uri?
        get() = getUriValue(CATEGORY_VIDEOS)
        set(value) = setNewUriValue(CATEGORY_VIDEOS, value)

    var preventBeDefault: Boolean
        get() = mDefaultPrefs.getBoolean(PREVENT_BE_DEFAULT, true)
        set(value) = setNewBooleanValue(PREVENT_BE_DEFAULT, value)
}
