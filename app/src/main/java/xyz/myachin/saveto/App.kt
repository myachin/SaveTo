package xyz.myachin.saveto

import android.app.Application
import xyz.myachin.saveto.settings.Settings

class App : Application() {
    override fun onCreate() {
        super.onCreate()
        Settings.setup(applicationContext)
    }
}