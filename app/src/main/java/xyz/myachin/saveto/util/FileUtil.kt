package xyz.myachin.saveto.util

class FileUtil {

    companion object {

        fun getBaseFileName(fileName: String): String {
            val index = fileName.lastIndexOf('.')
            return if (index == -1) {
                fileName
            } else {
                fileName.substring(0, index)
            }
        }

        fun getExtension(fileName: String): String? {
            val index = fileName.lastIndexOf('.')
            return if (index == -1) {
                null
            } else {
                fileName.substring(index + 1, fileName.length)
            }
        }

    }

}